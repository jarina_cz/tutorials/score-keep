/* Imports */
// Core
import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Components
import TitleBar from './TitleBar';
import AddPlayer from './AddPlayer';
import PlayerList from './PlayerList';

class App extends Component {
  render() {
    return (
      <div>
        <TitleBar title={this.props.title}/>
        <div className="wrapper">
          <AddPlayer/>
          <PlayerList players={this.props.players}/>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string.isRequired,
  players: PropTypes.array.isRequired
};
App.defaultProps = {};

export default App;
